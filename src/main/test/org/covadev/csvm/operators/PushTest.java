package org.covadev.csvm.operators;

import io.vavr.collection.List;
import io.vavr.control.Try;
import org.covadev.csvm.BoxedOperand;
import org.covadev.csvm.CFunc;
import org.covadev.csvm.Operator;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PushTest
{
    @Test
    public void PushTestTrue() {
        List<Operator> list = List.of(new Push(new BoxedOperand<>(true)));
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isSuccess());
        Assert.assertTrue(res.get());
    }
    @Test
    public void PushTestFalse() {
        List<Operator> list = List.of(new Push(new BoxedOperand<>(false)));
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isSuccess());
        Assert.assertFalse(res.get());
    }
}