package org.covadev.csvm.operators;

import io.vavr.collection.List;
import io.vavr.control.Try;
import org.covadev.csvm.AddressOutOfBoundsError;
import org.covadev.csvm.BoxedOperand;
import org.covadev.csvm.CFunc;
import org.covadev.csvm.EmptyMemoryError;
import org.covadev.csvm.EmptyStackError;
import org.covadev.csvm.MismatchedTypesError;
import org.covadev.csvm.NullOperand;
import org.covadev.csvm.Operator;
import org.covadev.csvm.UndefinedMemorySpaceError;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class LoadTest
{
    @Test
    public void LoadTest_EmptyStack() {
        List<Operator> list = List.of(new Load());
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof EmptyStackError);
    }

    @Test
    public void LoadTest_UndefinedMemory() {
        List<Operator> list = List.of(new Load());
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(null, Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof UndefinedMemorySpaceError);
    }

    @Test
    public void LoadTest_InvalidOperand_UsingNull() {
        List<Operator> list = List.of(
                new Push<>(new NullOperand())
                , new Load()
        );
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof MismatchedTypesError);
    }

    @Test
    public void LoadTest_InvalidOperand_UsingBool() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(true))
                , new Load()
        );
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof MismatchedTypesError);
    }

    @Test
    public void LoadTest_EmptyMemory() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(0))
                , new Load()
        );
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof EmptyMemoryError);
    }

    @Test
    public void LoadTest_InvalidAddress_LessThanZero() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(-1))
                , new Load()
        );
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.of("Hello","World"), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof AddressOutOfBoundsError);
    }

    @Test
    public void LoadTest_InvalidAddress_GreaterThanSize() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(2))
                , new Load()
        );
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.of("Hello","World"), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof AddressOutOfBoundsError);
    }

    @Test
    public void LoadTest_InvalidResult() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(0))
                , new Load()
        );
        CFunc func = new CFunc(list);
        Try<Boolean> res = func.call(List.of("Hello","World"), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof MismatchedTypesError);
    }

    @Test
    public void LoadTest_OK_Null() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(2))
                , new Load()
        );
        CFunc func = new CFunc(list);
        var res = func.call(List.of("Hello","World", null), null);
        Assert.assertTrue(res.isSuccess());
        Assert.assertNull(res.get());
    }

    @Test
    public void LoadTest_OK() {
        List<Operator> list = List.of(
                new Push<>(new BoxedOperand(0))
                , new Load()
        );
        CFunc func = new CFunc(list);
        var res = func.call(List.of("Hello","World", null), String.class);
        Assert.assertTrue(res.isSuccess());
        Assert.assertEquals(res.get(), "Hello");
    }

}