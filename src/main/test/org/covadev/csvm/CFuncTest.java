package org.covadev.csvm;

import io.vavr.collection.List;
import io.vavr.control.Try;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CFuncTest
{

    @Test
    public void testCall()
    {
        List<Operator> emptyList = List.empty();
        CFunc func = new CFunc(emptyList);
        Try<Boolean> res = func.call(List.empty(), Boolean.class);
        Assert.assertTrue(res.isFailure());
        Assert.assertTrue(res.getCause() instanceof NothingToReturnError );
    }
}