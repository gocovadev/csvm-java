package org.covadev.csvm;

public class MismatchedTypesError
        extends RuntimeError
{
    public <T> MismatchedTypesError(Class<T> expected, Class found) {
        super("Expected '"+expected+"' but found '"+found+"'");
    }
}
