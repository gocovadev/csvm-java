package org.covadev.csvm;

public class RuntimeError extends RuntimeException {
    public RuntimeError(String s) {
        super(s);
    }
    public RuntimeError(){}
}
