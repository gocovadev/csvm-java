package org.covadev.csvm;

import io.vavr.collection.List;
import io.vavr.control.Try;

import java.util.Stack;

public class CFunc
{
    private List<Operator> funcCode;
    public CFunc(Iterable<Operator> funcCode) {
        this.funcCode = List.ofAll(funcCode);
    }
    public <T> Try<T> call(List memory,Class<T> expectedResultClass, Stack<Operand> stack, Environment env) {
        try {
            if (env.programCounter < env.programSize) {
                int programCounterIncrement;
                Operator currOperator;
                do {
                    currOperator = funcCode.get(env.programCounter);
                    programCounterIncrement = currOperator.apply(stack, memory);
                    if (!(programCounterIncrement > 1 && currOperator instanceof Jmp)) {
                        env.programCounter++;
                    }
                    else {
                        env.programCounter += programCounterIncrement;
                    }
                }
                while (env.programCounter < env.programSize && !env.faulted);
            }
        }
        catch (Exception e) {
            env.faulted = true;
            return Try.failure(e);
        }
            if (stack.isEmpty()) {
                return Try.failure(new NothingToReturnError());
            }
            else {
                Operand finalOperand = stack.pop();
                if (finalOperand.getOperandClass() == expectedResultClass) {
                    if(!(finalOperand instanceof NullOperand)) {
                        return Try.success(((BoxedOperand<T>) finalOperand).unbox());
                    }
                    else {
                        return Try.success(null);
                    }
                }
                else {
                    return Try.failure(new MismatchedTypesError(expectedResultClass, finalOperand.getOperandClass()));
                }
            }
    }
    public <T> Try<T> call(List memory,Class<T> expectedResultClass) {
        Environment env = new Environment();
        env.programSize = funcCode.length();
        env.programCounter = 0;
        env.faulted = false;

        Stack<Operand> stack = new Stack<>();
        RuntimeError possibleError = null;
        return call(memory, expectedResultClass, stack, env);


    }
}
