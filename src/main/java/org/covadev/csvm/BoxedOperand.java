package org.covadev.csvm;

public class BoxedOperand<T>
        implements Operand
{
    private final Class boxedType;
    private final T value;

    public BoxedOperand(T value)
    {
        boxedType = value.getClass();
        this.value = value;
    }

    @Override
    public Class getOperandClass()
    {
        return boxedType;
    }

    public T unbox()
    {
        return value;
    }
}
