package org.covadev.csvm.operators;

import io.vavr.collection.List;
import org.covadev.csvm.AddressOutOfBoundsError;
import org.covadev.csvm.BoxedOperand;
import org.covadev.csvm.EmptyMemoryError;
import org.covadev.csvm.EmptyStackError;
import org.covadev.csvm.MismatchedTypesError;
import org.covadev.csvm.NullOperand;
import org.covadev.csvm.NullStackError;
import org.covadev.csvm.Operand;
import org.covadev.csvm.Operator;
import org.covadev.csvm.RuntimeError;
import org.covadev.csvm.UndefinedMemorySpaceError;

import java.util.Stack;

public class Load<T extends Operand>
        implements Operator
{
    @Override
    public Integer apply(Stack<Operand> operands, List memory)
    {
        if(operands == null ) throw new NullStackError();
        if(memory == null) throw new UndefinedMemorySpaceError();
        if(operands.isEmpty()) throw  new EmptyStackError();
        Operand addressOperand = operands.pop();
        if(addressOperand.getOperandClass() == Integer.class) {
            if(memory.isEmpty()) throw new EmptyMemoryError();
            var boxedOperand = (BoxedOperand<Integer>)addressOperand;
            int address = boxedOperand.unbox();
            int memorySize = memory.length();
            if(address>= 0 && address< memorySize) {
                var value = memory.get(address);
                if(value != null) {
                    operands.push(new BoxedOperand<>(value));
                }
                else {
                    operands.push(new NullOperand());
                }
            }
            else {
                throw  new AddressOutOfBoundsError("The address should be >=0 and < "+memorySize+", but "+address+" was provided");
            }
        }
        else {
            throw  new MismatchedTypesError(Integer.class, addressOperand.getOperandClass());
        }
        return 1;
    }
}
