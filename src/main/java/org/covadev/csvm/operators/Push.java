package org.covadev.csvm.operators;

import io.vavr.collection.List;
import org.covadev.csvm.NullStackError;
import org.covadev.csvm.Operand;
import org.covadev.csvm.Operator;
import org.covadev.csvm.RuntimeError;

import java.util.Stack;

public class Push<T extends Operand>
        implements Operator
{
    private T operand;
    public Push(T operand) {
        this.operand = operand;
    }

    @Override
    public Integer apply(Stack<Operand> operands, List memory)
    {
        if(operands == null ) throw new NullStackError();
        operands.push(operand);
        return 1;
    }

    public Push<T> Clone() {
        return new Push(operand);
    }
}
