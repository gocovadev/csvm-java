package org.covadev.csvm;

public class NullOperand
        implements Operand
{
    @Override
    public Class getOperandClass()
    {
        return null;
    }
}
