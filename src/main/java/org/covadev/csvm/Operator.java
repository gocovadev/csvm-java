package org.covadev.csvm;

import io.vavr.Function2;
import io.vavr.collection.List;

import java.util.Stack;

public interface Operator extends Function2<Stack<Operand>, List, Integer>, Cloneable {}
