package org.covadev.csvm;

public class AddressOutOfBoundsError
        extends RuntimeError
{
    public AddressOutOfBoundsError(String s) {super(s);}
}
